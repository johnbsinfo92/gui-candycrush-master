<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as GuzzleClient;
class Game extends Model
{
    protected $fillable = ['score'];
    protected $hidden = ['id'];
    protected $table = 'game';
    private $urlApi = 'http://apicandycrush.apphb.com/api/';

    public function GetAllGames(){
        $client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response = $client->get('Partidas');
        $body = json_decode($response->getBody(),true);
        return $body;
    }

    public function NewGame($request){
        $data=['score' => $request['score']];
        $client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response = $client->post('Partidas',['form_params'=>$data]);
        return $response->getStatusCode();
    }
}
