<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountServiceGoogle as SocialAccountServiceGoogle;
use Socialite;


class GoogleController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::with('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(SocialAccountServiceGoogle $service)
    {
        $user = $service->createOrGetUserGoogle(Socialite::driver('google')->user());
 
        auth()->login($user);

        return redirect()->to('/home');
        // $user->token;
    }
}