<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService as SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function faceRedirect()
    {
        return Socialite::with('facebook')->redirect();
    }

    public function faceCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());

        auth()->login($user);

        return redirect()->to('/home');
    }
}
