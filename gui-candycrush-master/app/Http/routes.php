<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=> 'auth'],function(){
	Route::resource('/users','UserController',['except'=>['create','store','show']]);
});
Route::resource('game','GameController');

Route::get('auth/google', 'GoogleController@redirectToProvider')->name('google.login');
Route::get('auth/callback', 'GoogleController@handleProviderCallback');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/redirect', 'SocialAuthController@faceRedirect');
Route::get('/callback/', 'SocialAuthController@faceCallback');