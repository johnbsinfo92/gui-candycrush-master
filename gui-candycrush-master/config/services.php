<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1410023369305097',
        'client_secret' => '81ad2bfcb63a4b01caf059c4d1213d68',
        'redirect' => 'http://localhost:8000/callback',
],

    'google' => [
    'client_id' => '611651698738-3hb36abg7efo4kep75fom4t2pfb2hflc.apps.googleusercontent.com',
    'client_secret' => 'CXxEJsrAs0g7tIFEog7o31FP',
    'redirect' => 'http://localhost:8000/auth/callback',
    //'redirect' => 'http://localhost/auth/callback/google',
],


];
