/**
 * Created by John Benavides on 18/8/2016.
 */
//crea un var para guardar los datos que están en los inputs de la vista.
var dataDTO = {
    UserID: $('#id_usuario').val(),
    PartidaID: $('#id_partida').val()
};
//llama al objeto Resource creado en el resource.js,
//llama al create y le pasamos el var donde guardamos los datos de la vista
//al .done es para devolver los datos despues de la llamada ajax y así agregarlos a la tabla con el .append
UserPartidaResource.create(dataDTO, true).done(function (partida_usuario) {
    document.getElementById('name').value = partida_usuario.User.name;
    console.log(JSON.stringify(partida_usuario));
}).fail(function (error) {
    console.log(error);
    alert("Ha ocurrido un error");
});