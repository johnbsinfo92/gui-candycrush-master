@extends('layouts.app')
@section('content')
        <h2>Striped Rows</h2>
        <p>The .table-striped class adds zebra-stripes to a table:</p>
        <a href="{{url('/game/create')}}">New Game</a>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ObjGame as $listScore)
                <tr>
                    <td>{{$listScore['score']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

@stop