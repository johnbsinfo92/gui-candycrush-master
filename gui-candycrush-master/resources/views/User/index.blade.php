@extends('layouts.app')
@section('content')
        <h2>List of Users</h2>
        <p>This is a list of all the users in the sistem from the api</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($UserList as $listU)
                <tr>
                    <td>{{$listU['name']}}</td>
                    <td>{{$listU['email']}}</td>
                    <td>
                        <div class="der">
                            <form action="/users/{{$listU['id']}}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                {!! method_field('DELETE') !!}
                                <button type="submit" class="btn btn-default" aria-label="Left Align">
                                  <span class="glyphicon glyphicon-oil" aria-hidden="true"></span>
                                </button>
                            </form>
                            <a href="/users/{{$listU['id']}}/edit"><i class="glyphicon glyphicon-pencil"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
@stop